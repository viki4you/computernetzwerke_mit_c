#define MAX_PKT 1024                                    /* Bestimmt die Packetgröße in Byte */

typedef enum {false, true} boolean;                     /* Boolescher Typ */
typedef unsigned int seq_nr;                            /* Folge- oder Bestätigungsnummer */

// Möglicher Fehler, im Buch steht 'paket' statt 'packet'(Ohne c in paket).
typedef struct {unsigned char data[MAX_PKT];} packet;   /* Paketdefinition */
typedef enum {data, ack, nak} frame_kind;               /* Definition der Rahmenart */

typedef struct {                                        /* Rahmen, die auf dieser Schicht übertragen werden */
    frame_kind kind;                                    /* Welche Art von Rahmen */
    seq_nr seq;                                         /* Folgenummer */
    seq_nr ack;                                         /* Bestätigungsnummer */
    packet info;                                        /* Paket der Vermittlungsschicht */
} frame;

/* Auf Ergebnis warten; Typ in event zurückgeben */
void wait_for_event(event_type *event);

/* Paket von der Vermittlungsschicht zur Übertragung holen */
void from_network_layer(packet *p, int i);

/* Informationen eines eingegangenen Rahmens zur Vermittlungsschichct senden */
void to_network_layer(packet *p);

/* Eingegangenen Rahmen von der Bitübertragungsschicht holen und auf r kopieren */
void from_physical_layer(frame *r, int i, frame *physical_layer);

/* Rahmen an Bitübertragungsschicht zur Übertragung weitergeben */
void to_physical_layer(frame *s, int i, frame *physical_layer);

/* Uhr starten und Timeout-Ereignis aktivieren */
void start_timer(seq_nr k);

/* Uhr anhalten und Timeout-Ereignis deaktivieren */
void stop_timer(seq_nr k);

/* Start eines Hilfstimers und Ereignis ack_timeout aktivieren */
void start_ack_timer(void);

/* Hilfstimer anhalten und das Ereignis ack_timeout deaktivieren */
void stop_ack_timer(void);

/* Vermittlungsschicht erlauben, das Ereignis network_layer_ready auszulösen. */
void enable_network_layer(void);

/* Vermittlungsschicht verbieten, das Ereignis network_layer_ready auszulösen */
void disable_network_layer(void);

/* Makro inc wird in der Zeile erweitert: k wird inkrementiert (modulo MAX_SEQ). */
#define inc(k) if (k < MAX_SEQ) k = k + 1; else k = 0
