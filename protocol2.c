/* protocol2.c ist eine Kopie von protocol1.c mit dem Zusatz
eines Threads.

protocol2.c hat zwei Threads um *event dynamisch zu verändern.
Das wirkt sich wait_for_event aus. wait_for_event läuft so lange
in einer Endlosschleife, bis *event durch den anderen Thread 
verändert wird. Dadurch wird die Frameübertragung ausgelöst. PHYSICAL_LAYER_SIZE
 Frames werden gesendet (Default ist 10). */

typedef enum {frame_arrival, no_frame} event_type;

#include "protocol.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

const int PHYSICAL_LAYER_SIZE = 10;

event_type event = frame_arrival;

/* Funktionen Deklaration */
frame* createArray(int);

void sender1(frame*);

void receiver1(frame*, event_type*);

void setTimeout(int);

void *myThreadFun(void *);

void *startProtocol(void *);

main() {
    event_type event;
    
    pthread_t protocolThread;
    pthread_t tid;
    printf("Before Thread\n");
    pthread_create(&protocolThread, NULL, startProtocol, (void *)&event);
    pthread_create(&tid, NULL, myThreadFun, (void *)&event);
    pthread_join(tid, NULL);
    printf("After Thread\n");
}

/* Auf Ergebnis warten; Typ in event zurückgeben */
void wait_for_event(event_type *event){
    printf("wait_for_event before while event = %d\n", *event);
    while(*event == 0){
        printf("wait_for_event event = %d\n", *event);
    }
    printf("wait_for_event after while event = %d\n", *event);
}

/* Paket von der Vermittlungsschicht zur Übertragung holen */
void from_network_layer(packet *p, int packetNum){
    unsigned char packetInhalt[MAX_PKT] = "Ich bin der Inhalt des Packets Nummer ";
    char numStr[12];
    sprintf(numStr, "%d", packetNum);
    strcat(packetInhalt, numStr);
    strcpy(p->data, packetInhalt);
}

/* Informationen eines eingegangenen Rahmens zur Vermittlungsschicht senden */
void to_network_layer(packet *p){
    printf("to_network_layer %s\n", p->data);
}

/* Eingegangenen Rahmen von der Bitübertragungsschicht holen und auf r kopieren */
void from_physical_layer(frame *r, int frameNum, frame *physical_layer){
    *r = *(physical_layer + frameNum);
}

/* Rahmen an Bitübertragungsschicht zur Übertragung weitergeben */
void to_physical_layer(frame *s, int frameNum, frame *physical_layer){
    *(physical_layer + frameNum) = *s;
}

/* Funktion beschreibung reserviere Speicher */
frame* createArray(int count) {
    frame* a;
    a = malloc(sizeof(frame) * count);
    if (a == 0) {
        fprintf(stderr, "memory allocation failed\n");
        exit(1);
    }
    return a;
}

void sender1(frame* physical_layer){
    frame s;                                    /* Puffer für ausgehenden Rahmen */
    packet buffer;                              /* Puffer für ausgehendes Paket */
    
    int i = 0;
    while(i < PHYSICAL_LAYER_SIZE){                              /* Erstelle 10 Packete mit fortlaufender Nummerierung */
        from_network_layer(&buffer, i);         /* Hole etwas zum Senden */
        s.info = buffer;                        /* Kopiere es in s zum Übertragen */
        to_physical_layer(&s, i, physical_layer);               /* Sende es ab */
        i++;
    }
}

void receiver1(frame* physical_layer, event_type* event){
    frame r;
    packet buffer;
    
    int i = 0;
    while(i < PHYSICAL_LAYER_SIZE){
        wait_for_event(event);
        from_physical_layer(&r, i, physical_layer);
        buffer = r.info;
        to_network_layer(&buffer);
        i++;
    }
}

void setTimeout(int milliseconds)
{
    // If milliseconds is less or equal to 0
    // will be simple return from function without throw error
    if (milliseconds <= 0) {
        fprintf(stderr, "Count milliseconds for timeout is less or equal to 0\n");
        return;
    }

    // a current time of milliseconds
    int milliseconds_since = clock() * 1000 / CLOCKS_PER_SEC;

    // needed count milliseconds of return from this timeout
    int end = milliseconds_since + milliseconds;

    // wait while until needed time comes
    do {
        milliseconds_since = clock() * 1000 / CLOCKS_PER_SEC;
    } while (milliseconds_since <= end);
}

void *myThreadFun(void *vargp)
{
    event_type *my_event = (event_type *)vargp;
    sleep(1);
    *my_event = 1;
    printf("myThreadFun event = %d\n", *my_event);
    printf("Printing GeeksQuiz from Thread \n");
    return NULL;
}

void *startProtocol(void *vargp)
{
    printf("You are inside protocol2.c!\n");
    frame* physical_layer = createArray(PHYSICAL_LAYER_SIZE);
    
    event_type *my_event = (event_type *)vargp;
    
    sender1(physical_layer);
    receiver1(physical_layer, my_event);
    free(physical_layer);
    return NULL;
}