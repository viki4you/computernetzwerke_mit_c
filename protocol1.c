typedef enum {frame_arrival} event_type;
#include "protocol.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const int PHYSICAL_LAYER_SIZE = 10;

void *myThreadFun(void *vargp)
{
    sleep(1);
    printf("Printing GeeksQuiz from Thread \n");
    return NULL;
}

frame* createArray(int count) {
    frame* a;
    a = malloc(sizeof(frame) * count);
    if (a == 0) {
        fprintf(stderr, "memory allocation failed\n");
        exit(1);
    }
    return a;
}

void sender1(frame* physical_layer){
    frame s;                                    /* Puffer für ausgehenden Rahmen */
    packet buffer;                              /* Puffer für ausgehendes Paket */
    
    int i = 0;
    while(i < PHYSICAL_LAYER_SIZE){                              /* Erstelle 10 Packete mit fortlaufender Nummerierung */
        from_network_layer(&buffer, i);         /* Hole etwas zum Senden */
        s.info = buffer;                        /* Kopiere es in s zum Übertragen */
        to_physical_layer(&s, i, physical_layer);               /* Sende es ab */
        i++;
    }
}

void receiver1(frame* physical_layer){
    frame r;
    event_type event;                           /* Von wait ausgefüllt, aber hier nicht verwendet */
    packet buffer;
    
    int i = 0;
    while(i < PHYSICAL_LAYER_SIZE){
        wait_for_event(&event);                 /* Einzige Möglichkeit ist frame_arrival */
        from_physical_layer(&r, i, physical_layer);
        buffer = r.info;
        to_network_layer(&buffer);
        i++;
    }
}

main() {
    printf("You are inside protocol1.c!\n");
    frame* physical_layer = createArray(PHYSICAL_LAYER_SIZE);
    sender1(physical_layer);
    receiver1(physical_layer);
    free(physical_layer);
}

/* Auf Ergebnis warten; Typ in event zurückgeben */
void wait_for_event(event_type *event){
    
}

/* Paket von der Vermittlungsschicht zur Übertragung holen */
void from_network_layer(packet *p, int packetNum){
    unsigned char packetInhalt[MAX_PKT] = "Ich bin der Inhalt des Packets Nummer ";
    char numStr[12];
    sprintf(numStr, "%d", packetNum);
    strcat(packetInhalt, numStr);
    strcpy(p->data, packetInhalt);
}

/* Informationen eines eingegangenen Rahmens zur Vermittlungsschicht senden */
void to_network_layer(packet *p){
    printf("to_network_layer %s\n", p->data);
}

/* Eingegangenen Rahmen von der Bitübertragungsschicht holen und auf r kopieren */
void from_physical_layer(frame *r, int frameNum, frame *physical_layer){
    *r = *(physical_layer + frameNum);
}

/* Rahmen an Bitübertragungsschicht zur Übertragung weitergeben */
void to_physical_layer(frame *s, int frameNum, frame *physical_layer){
    *(physical_layer + frameNum) = *s;
}
